<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Patient_model extends CI_Model{

	const TBL_BUS = 'patientinfo';
	//构造函数

	
	/*
	 * 取得所有信息
	 * 返回：所有信息
	 */
	public function getAll($id,$startPage,$endPage){

		$condition=array(
				'doctorid'=>$id,
		);
		$query=$this->db->where($condition)->order_by('id DESC')->limit($startPage, $endPage)->get(self::TBL_BUS);
		return $query->result_array();
	}

	public function getAll2($id){

		$condition=array(
				'doctorid'=>$id,
		);
		$query=$this->db->where($condition)->order_by('id DESC')->get(self::TBL_BUS);
		return $query->result_array();
	}

	public function getAll3($id,$search){
		$condition1=array(
				'doctorid'=>$id
		);
		$condition2=array(
				'mobile'=>$search
		);
		$query=$this->db->like($condition2)->where($condition1)->order_by('id DESC')->get(self::TBL_BUS);
		return $query->result_array();
	}


	public function getAll4(){

		$condition1=array(
				'patientinfo.type'=>'单条添加'
		);

		$this->db->select('patientinfo.id,patientinfo.mobile as pmobile,patientinfo.name,patientinfo.sex,patientinfo.binzhong,patientinfo.status,patientinfo.beizhu,patientinfo.dateTime,patientinfo.doctorid,,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('patientinfo');
		$this->db->join('doctorinfo', 'patientinfo.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'patientinfo.doctorid = doctorinfoJ.id','left');
		$this->db->order_by('patientinfo.id ASC');
		$this->db->where($condition1);
		$query = $this->db->get();
		return $query->result_array();
		// $query=$this->db->order_by('id DESC')->where($condition1)->get(self::TBL_BUS);
		// return $query->result_array();
	}


	public function getAll5($id,$search){
		$condition1=array(
				'doctorid'=>$id
		);
		$condition2=array(
				'name'=>$search
		);
		$query=$this->db->like($condition2)->where($condition1)->order_by('id DESC')->get(self::TBL_BUS);
		return $query->result_array();
	}


	public function getAll6($search){

		$condition1=array(
				'patientinfo.type'=>'单条添加'
		);

		$condition2=array(
				'patientinfo.name'=>$search
		);

		$this->db->select('patientinfo.id,patientinfo.mobile as pmobile,patientinfo.name,patientinfo.sex,patientinfo.binzhong,patientinfo.status,patientinfo.beizhu,patientinfo.dateTime,patientinfo.doctorid,,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('patientinfo');
		$this->db->join('doctorinfo', 'patientinfo.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'patientinfo.doctorid = doctorinfoJ.id','left');
		$this->db->order_by('patientinfo.id ASC');
		$this->db->where($condition1);
		$this->db->like($condition2);
		$query = $this->db->get();
		return $query->result_array();
		// $query=$this->db->order_by('id DESC')->where($condition1)->get(self::TBL_BUS);
		// return $query->result_array();
	}

	public function getAll7($search){

		$condition1=array(
				'patientinfo.type'=>'单条添加'
		);

		$condition2=array(
				'patientinfo.mobile'=>$search
		);

		$this->db->select('patientinfo.id,patientinfo.mobile as pmobile,patientinfo.name,patientinfo.sex,patientinfo.binzhong,patientinfo.status,patientinfo.beizhu,patientinfo.dateTime,patientinfo.doctorid,,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('patientinfo');
		$this->db->join('doctorinfo', 'patientinfo.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'patientinfo.doctorid = doctorinfoJ.id','left');
		$this->db->order_by('patientinfo.id ASC');
		$this->db->where($condition1);
		$this->db->like($condition2);
		$query = $this->db->get();
		return $query->result_array();
		// $query=$this->db->order_by('id DESC')->where($condition1)->get(self::TBL_BUS);
		// return $query->result_array();
	}


	public function getAll8(){

		$condition1=array(
				'patientinfo.type'=>'批量导入'
		);

		$this->db->select('patientinfo.id,patientinfo.mobile as pmobile,patientinfo.name,patientinfo.sex,patientinfo.binzhong,patientinfo.status,patientinfo.beizhu,patientinfo.dateTime,patientinfo.doctorid,,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('patientinfo');
		$this->db->join('doctorinfo', 'patientinfo.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'patientinfo.doctorid = doctorinfoJ.id','left');
		$this->db->order_by('patientinfo.id ASC');
		$this->db->where($condition1);
		$query = $this->db->get();
		return $query->result_array();
		// $query=$this->db->order_by('id DESC')->where($condition1)->get(self::TBL_BUS);
		// return $query->result_array();
	}


	public function getAll9($search){

		$condition1=array(
				'patientinfo.type'=>'批量导入'
		);

		$condition2=array(
				'patientinfo.name'=>$search
		);

		$this->db->select('patientinfo.id,patientinfo.mobile as pmobile,patientinfo.name,patientinfo.sex,patientinfo.binzhong,patientinfo.status,patientinfo.beizhu,patientinfo.dateTime,patientinfo.doctorid,,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('patientinfo');
		$this->db->join('doctorinfo', 'patientinfo.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'patientinfo.doctorid = doctorinfoJ.id','left');
		$this->db->order_by('patientinfo.id ASC');
		$this->db->where($condition1);
		$this->db->like($condition2);
		$query = $this->db->get();
		return $query->result_array();
		// $query=$this->db->order_by('id DESC')->where($condition1)->get(self::TBL_BUS);
		// return $query->result_array();
	}


	public function getAll10($search){

		$condition1=array(
				'patientinfo.type'=>'批量导入'
		);

		$condition2=array(
				'patientinfo.mobile'=>$search
		);

		$this->db->select('patientinfo.id,patientinfo.mobile as pmobile,patientinfo.name,patientinfo.sex,patientinfo.binzhong,patientinfo.status,patientinfo.beizhu,patientinfo.dateTime,patientinfo.doctorid,,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('patientinfo');
		$this->db->join('doctorinfo', 'patientinfo.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'patientinfo.doctorid = doctorinfoJ.id','left');
		$this->db->order_by('patientinfo.id ASC');
		$this->db->where($condition1);
		$this->db->like($condition2);
		$query = $this->db->get();
		return $query->result_array();
		// $query=$this->db->order_by('id DESC')->where($condition1)->get(self::TBL_BUS);
		// return $query->result_array();
	}

	//增加一条记录
	public function addPatientOne($data){
		//如果用户名合法
			return $this->db->insert(self::TBL_BUS,$data);
	}


	//删除一个医生账号
	public function deleteDoctorOne($id)
	{
		return $this->db->delete(self::TBL_BUS,array('id'=>$id));
	}

		 /*
	 * 根据用户名和密码验证管理员是否存在
	 * @param 商家账号，密码
	 * 返回值：存在true，不存在false
	 */
	public function cheAdminByNP($admName){
		$condition=array(
			'mobile'=>$admName,
			'type'=>'单个添加'
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->num_rows()>0 ? true:false;
	}

	/*
	* 取得一条医生信息
	*/
	public function getPatientOne($id){
		$condition=array(
				'id'=>$id,
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->row_array();
	}

	/*
	* 取得一条医生信息
	*/
	public function getPatientByMobile($mobile){
		$condition=array(
				'mobile'=>$mobile,
				'type'=>'单条添加'
		);

		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->row_array();
	}

	/*
	 * 更新一条商家信息
	 */
	public function updateOne($conid,$data)
	{
		$condition=array(
				'id' => $conid,
		);
		return $query=$this->db->where($condition)->update(self::TBL_BUS,$data);
	}


}