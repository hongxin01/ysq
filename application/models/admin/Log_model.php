<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log_model extends CI_Model{

	const TBL_BUS = 'logAll';
	//构造函数
	public function __construct(){
		//调用父类构造函数，必不可少
		parent::__construct();
		//手动载入数据库操作类
		$this->load->database();
	}
	
	/*
	 * 取得所有商家信息
	 * 返回：所有的商家信息
	 */
	public function getLogAll(){
		$query=$this->db->get(self::TBL_BUS);
		return $query->result_array();
	}
	
}