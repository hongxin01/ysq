<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{

	const TBL_BUS = 'admin';
	//构造函数
	public function __construct(){
		//调用父类构造函数，必不可少
		parent::__construct();
		//手动载入数据库操作类
		$this->load->database();
	}
	
	/*
	 * 取得所有商家信息
	 * 返回：所有的商家信息
	 */
	public function getAdminAll(){
		$query=$this->db->get(self::TBL_BUS);
		return $query->result_array();
	}

    /*
	 * 根据用户名和密码验证管理员是否存在
	 * @param 商家账号，密码
	 * 返回值：存在true，不存在false
	 */
	public function cheAdminByNP($admName,$admPwd){
		$condition=array(
			'name'=>$admName,
			'pwd'=>$admPwd	
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->num_rows()>0 ? true:false;
	}

	public function getDoctorOne($name){
		$condition=array(
				'name'=>$name
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->row_array();
	}


	public function updateOne($conid,$data)
	{
		$condition=array(
				'id' => $conid,
		);
		return $query=$this->db->where($condition)->update(self::TBL_BUS,$data);
	}
	
	
}