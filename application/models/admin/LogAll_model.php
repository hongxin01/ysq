<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LogAll_model extends CI_Model{

	const TBL_BUS = 'logAll';
	//构造函数
	public function __construct(){
		//调用父类构造函数，必不可少
		parent::__construct();
		//手动载入数据库操作类
		$this->load->database();
	}
	
	public function getLogAll(){
		$query=$this->db->order_by('id DESC')->get(self::TBL_BUS);
		return $query->result_array();
	}

	public function addOne($data){
		return $this->db->insert(self::TBL_BUS,$data);
	}

	//按照医生名字搜索
	public function getDoByName($name)
	{
		$condition=array(
				'person'=>$name,
		);
		
		$query=$this->db->like($condition)->get(self::TBL_BUS);
		return $query->result_array();
	}
	
}