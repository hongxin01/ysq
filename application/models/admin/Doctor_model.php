<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doctor_model extends CI_Model{

	const TBL_BUS = 'doctorinfo';
	//构造函数
	public function __construct(){
		//调用父类构造函数，必不可少
		parent::__construct();
		//手动载入数据库操作类
		$this->load->database();
	}
	
	/*
	 * 取得所有医生信息
	 * 返回：所有的医生信息
	 */
	public function getDoctorAll(){
		//$query=$this->db->get(self::TBL_BUS);
		//return $query->result_array();
		$this->db->select('doctorinfo.id,doctorinfo.loginName,doctorinfo.weichat,doctorinfo.mobile,doctorinfo.realName,doctorinfo.group,doctorinfo.hostpital,doctorinfo.depar,doctorinfo.dateTime,doctorinfo.doctorid,groupD.name');
		$this->db->from('doctorinfo');
		$this->db->join('groupD', 'doctorinfo.group = groupD.id');
		$this->db->order_by('doctorinfo.id ASC');
		$query = $this->db->get();
		return $query->result_array();

	}

	//按照医生名字搜索
	public function getDoByName($name)
	{
		$condition=array(
				'doctorinfo.loginName'=>$name,
		);
		
		// $query=$this->db->like($condition)->get(self::TBL_BUS);
		// return $query->result_array();
		$this->db->select('*');
		$this->db->from('doctorinfo');
		$this->db->join('groupD', 'doctorinfo.group = groupD.id');
		$this->db->order_by('doctorinfo.id ASC');
		$this->db->like($condition);
		$query = $this->db->get();
		return $query->result_array();
	}

	//根据日期进行搜索
	public function getDoByTime($startime,$endtime)
	{
		$condition="doctorinfo.dateTime between '$startime' and '$endtime'";
		// $query=$this->db->like($condition)->get(self::TBL_BUS);
		// return $query->result_array();
		$this->db->select('*');
		$this->db->from('doctorinfo');
		$this->db->join('groupD', 'doctorinfo.group = groupD.id');
		$this->db->order_by('doctorinfo.id ASC');
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->result_array();
	}


	//按照医生名字搜索
	public function getDoByGroup($name)
	{
		$condition=array(
				'doctorinfo.group'=>$name,
		);
		
		// $query=$this->db->like($condition)->get(self::TBL_BUS);
		// return $query->result_array();
		$this->db->select('*');
		$this->db->from('doctorinfo');
		$this->db->join('groupD', 'doctorinfo.group = groupD.id');
		$this->db->order_by('doctorinfo.id ASC');
		$this->db->like($condition);
		$query = $this->db->get();
		return $query->result_array();
	}


	/*
	* 增加一条医生信息
	*/
	public function addDoctorByN($name,$data){
		//如果用户名合法
		
		if(!$this->isNameOk($name)){
			return $this->db->insert(self::TBL_BUS,$data);
			//return 1;
		}else{
			return 0;
		}
	}


	/*
	* 取得一条医生信息
	*/
	public function getDoctorOne($id){
		$condition=array(
				'id'=>$id,
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->row_array();
	}


	/*
	* 取得一条医生信息
	*/
	public function getOneByOpenid($openid){

		$condition=array(
				'doctorinfo.weichat'=>$openid,
		);
		
		$this->db->select('*');
		$this->db->from('doctorinfo');
		$this->db->join('groupD', 'doctorinfo.id = groupD.id');
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->row_array();
	}


	/*
	* 取得一条医生信息
	*/
	public function getOneByNP($admName,$admPwd){
		$condition=array(
			'loginName'=>$admName,
			'pwd'=>$admPwd	
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->row_array();
	}

	//判断账号是否存在
	public function isNameOk($name)
	{
		//先判断是否已经存在账号
		$condition=array(
			'loginName'=>$name,
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->num_rows()>0 ? true:false;
	}

	//删除一个医生账号
	public function deleteDoctorOne($id)
	{
		return $this->db->delete(self::TBL_BUS,array('id'=>$id));
	}
	

	 /*
	 * 根据用户名和密码验证管理员是否存在
	 * @param 商家账号，密码
	 * 返回值：存在true，不存在false
	 */
	public function cheAdminByNP($admName,$admPwd){
		$condition=array(
			'loginName'=>$admName,
			'pwd'=>$admPwd	
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->num_rows()>0 ? true:false;
	}

		 /*
	 * 根据用户名和密码验证管理员是否存在
	 * @param 商家账号，密码
	 * 返回值：存在true，不存在false
	 */
	public function cheByOpenid($openid){
		$condition=array(
			'weichat'=>$openid,
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->num_rows()>0 ? true:false;
	}

	/*
	 * 更新一条商家信息
	 */	
	public function updateOne($conid,$data)
	{
		$condition=array(
				'id' => $conid,
		);
		return $query=$this->db->where($condition)->update(self::TBL_BUS,$data);
	}

	/*
	 * 更新一条商家信息
	 */	
	public function updateOne2($conid,$data)
	{
		$condition=array(
				'doctorid' => $conid,
		);
		return $query=$this->db->where($condition)->update(self::TBL_BUS,$data);
	}


	public function cheGroup($groupid){
		$condition=array(
			'group'=>$groupid
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->num_rows()>0 ? true:false;
	}
	
	public function cheWeichat($weichat){
		$condition=array(
			'weichat'=>$weichat
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->num_rows()>0 ? true:false;
	}



}