<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DoctorJ_model extends CI_Model{

	const TBL_BUS = 'doctorinfoJ';
	//构造函数
	public function __construct(){
		//调用父类构造函数，必不可少
		parent::__construct();
		//手动载入数据库操作类
		$this->load->database();
	}
	
			 /*
	 * 根据用户名和密码验证管理员是否存在
	 * @param 商家账号，密码
	 * 返回值：存在true，不存在false
	 */
	public function cheByOpenid($openid){
		$condition=array(
			'weichatJ'=>$openid,
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->num_rows()>0 ? true:false;
	}

	/*
	* 增加一条医生信息
	*/
	public function addOne($data){
		return $this->db->insert(self::TBL_BUS,$data);
	}


	public function getOneByOpenid($openid){

		$condition=array(
				'weichatj'=>$openid,
		);

		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->row_array();
	}


	public function getDoctorOne($id){
		$condition=array(
				'id'=>$id
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->row_array();
	}

			/*
	 * 更新一条商家信息
	 */
	public function updateOne($conid,$data)
	{
		$condition=array(
				'id' => $conid,
		);
		return $query=$this->db->where($condition)->update(self::TBL_BUS,$data);
	}


}