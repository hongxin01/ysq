<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group_model extends CI_Model{

	const TBL_BUS = 'groupD';
	//构造函数
	public function __construct(){
		//调用父类构造函数，必不可少
		parent::__construct();
		//手动载入数据库操作类
		$this->load->database();
	}
	
	/*
	 * 取得所有信息
	 * 返回：所有信息
	 */
	public function getGroupAll(){
		$query=$this->db->get(self::TBL_BUS);
		return $query->result_array();
	}


	//按照医生名字搜索
	public function getDoByName($name)
	{
		$condition=array(
				'name'=>$name,
		);
		
		$query=$this->db->like($condition)->get(self::TBL_BUS);
		return $query->result_array();
	}

	/*
	* 增加一条医生信息
	*/
	public function addOne($data){
		return $this->db->insert(self::TBL_BUS,$data);
	}


	//删除一个医生账号
	public function deleteDoctorOne($id)
	{
		return $this->db->delete(self::TBL_BUS,array('id'=>$id));
	}

		/*
	* 取得一条医生信息
	*/
	public function getDoctorOne($id){
		$condition=array(
				'id'=>$id,
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->row_array();
	}

		/*
	 * 更新一条商家信息
	 */
	public function updateOne($conid,$data)
	{
		$condition=array(
				'id' => $conid,
		);
		return $query=$this->db->where($condition)->update(self::TBL_BUS,$data);
	}


}