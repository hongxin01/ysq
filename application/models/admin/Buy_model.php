<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buy_model extends CI_Model{

	const TBL_BUS = 'buyRecord';
	//构造函数
	public function __construct(){
		//调用父类构造函数，必不可少
		parent::__construct();
		//手动载入数据库操作类
		$this->load->database();
	}
	
	//增加一条记录
	public function addBuyRecord($data){
		//如果用户名合法
			return $this->db->insert(self::TBL_BUS,$data);
	}


	/*
	 * 取得所有信息
	 * 返回：所有信息
	 */
	public function getBuyAll(){

		$this->db->select('buyRecord.id,buyRecord.name,buyRecord.mobil,buyRecord.address,buyRecord.timeb,buyRecord.product,buyRecord.oprice,buyRecord.number,buyRecord.aprice,buyRecord.beizhu,buyRecord.doctorid,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('buyRecord');
		$this->db->join('doctorinfo', 'buyRecord.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'buyRecord.doctorid = doctorinfoJ.id','left');
		$this->db->order_by('buyRecord.id ASC');
		$query = $this->db->get();
		return $query->result_array();
	}


	public function doctorGetBuyAll2($doctorid){
		$condition=array(
				'doctorid'=>$doctorid
		);
		$query=$this->db->like($condition)->get(self::TBL_BUS);
		return $query->result_array();
	}

	//删除一个医生账号
	public function deleteDoctorOne($id)
	{
		return $this->db->delete(self::TBL_BUS,array('id'=>$id));
	}


	/*
	* 取得一条医生信息
	*/
	public function getDoctorOne($id){
		$condition=array(
				'id'=>$id,
		);
		
		$query=$this->db->where($condition)->get(self::TBL_BUS);
		return $query->row_array();
	}

	/*
	 * 更新一条商家信息
	 */	
	public function updateOne($conid,$data)
	{
		$condition=array(
				'id' => $conid,
		);
		return $query=$this->db->where($condition)->update(self::TBL_BUS,$data);
	}

	//按照医生名字搜索
	public function getDoByName($name)
	{
		$condition=array(
				'buyRecord.name'=>$name,
		);

		$this->db->select('buyRecord.id,buyRecord.name,buyRecord.mobil,buyRecord.address,buyRecord.timeb,buyRecord.product,buyRecord.oprice,buyRecord.number,buyRecord.aprice,buyRecord.beizhu,buyRecord.doctorid,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('buyRecord');
		$this->db->join('doctorinfo', 'buyRecord.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'buyRecord.doctorid = doctorinfoJ.id','left');
		$this->db->like($condition);
		$this->db->order_by('buyRecord.id ASC');
		$query = $this->db->get();
		return $query->result_array();

	}

		//按照医生名字搜索
	public function getDoByMobile($name)
	{
		$condition=array(
				'buyRecord.mobil'=>$name,
		);

		$this->db->select('buyRecord.id,buyRecord.name,buyRecord.mobil,buyRecord.address,buyRecord.timeb,buyRecord.product,buyRecord.oprice,buyRecord.number,buyRecord.aprice,buyRecord.beizhu,buyRecord.doctorid,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('buyRecord');
		$this->db->join('doctorinfo', 'buyRecord.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'buyRecord.doctorid = doctorinfoJ.id','left');
		$this->db->like($condition);
		$this->db->order_by('buyRecord.id ASC');
		$query = $this->db->get();
		return $query->result_array();

	}

	public function getDoByDoctorN($name)
	{
		$condition=array(
				'doctorinfo.realName'=>$name,
		);

		$this->db->select('buyRecord.id,buyRecord.name,buyRecord.mobil,buyRecord.address,buyRecord.timeb,buyRecord.product,buyRecord.oprice,buyRecord.number,buyRecord.aprice,buyRecord.beizhu,buyRecord.doctorid,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('buyRecord');
		$this->db->join('doctorinfo', 'buyRecord.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'buyRecord.doctorid = doctorinfoJ.id','left');
		$this->db->like($condition);
		$this->db->order_by('buyRecord.id ASC');
		$query = $this->db->get();
		return $query->result_array();

	}

		//根据日期进行搜索
	public function getDoByTime($startime,$endtime)
	{
		$condition="buyRecord.timeb between '$startime' and '$endtime'";
		// $query=$this->db->like($condition)->get(self::TBL_BUS);
		// return $query->result_array();
		$this->db->select('buyRecord.id,buyRecord.name,buyRecord.mobil,buyRecord.address,buyRecord.timeb,buyRecord.product,buyRecord.oprice,buyRecord.number,buyRecord.aprice,buyRecord.beizhu,buyRecord.doctorid,doctorinfo.loginName,doctorinfo.mobile,doctorinfo.realName,doctorinfoJ.mobileJ,doctorinfoJ.realNameJ');
		$this->db->from('buyRecord');
		$this->db->join('doctorinfo', 'buyRecord.doctorid = doctorinfo.doctorid','left');
		$this->db->join('doctorinfoJ', 'buyRecord.doctorid = doctorinfoJ.id','left');
		$this->db->where($condition);
		$this->db->order_by('buyRecord.id ASC');
		$query = $this->db->get();
		return $query->result_array();
	}


	public function DoctorGetAllByTime($startime,$endtime,$id)
	{
		$condition="timeb between '$startime' and '$endtime'";
		$condition2=array(
				'mobil'=>$id,
		);
		$this->db->where($condition);
		$this->db->like($condition2);
		$this->db->order_by('id ASC');
		$query = $this->db->get(self::TBL_BUS);
		return $query->result_array();
	}


	public function DoctorGetAllByTime2($startime,$endtime,$id)
	{
		$condition="timeb between '$startime' and '$endtime'";
		$condition2=array(
				'doctorid'=>$id,
		);
		$this->db->where($condition);
		$this->db->like($condition2);
		$this->db->order_by('id ASC');
		$query = $this->db->get(self::TBL_BUS);
		return $query->result_array();
	}
	

	public function DoctorGetByName($name,$doctorid)
	{
		$condition=array(
				'name'=>$name,
				'doctorid'=>$doctorid

		);
		// $query=$this->db->like($condition)->get(self::TBL_BUS);
		// return $query->result_array();
		$this->db->like($condition);
		$this->db->order_by('id ASC');
		$query = $this->db->get(self::TBL_BUS);
		return $query->result_array();
	}

	public function DoctorGetByMobile($name,$doctorid)
	{
		$condition=array(
				'mobil'=>$name,
				'doctorid'=>$doctorid
		);
		$this->db->like($condition);
		$this->db->order_by('id ASC');
		$query = $this->db->get(self::TBL_BUS);
		return $query->result_array();
	}


	/*
	 * 取得所有信息
	 * 返回：所有信息
	 */
	public function doctorGetBuyAll($name){
		$condition=array(
				'mobil'=>$name,
		);
		$query=$this->db->like($condition)->get(self::TBL_BUS);
		return $query->result_array();
	}


}