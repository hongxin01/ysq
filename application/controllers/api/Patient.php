<?php
/**
 * 后台管理员登录
 */
defined('BASEPATH') OR exit("No direct script access allowed");

class Patient extends CI_Controller{


	//构造初始化函数
	public function __construct(){
		parent::__construct();

		//加载数据库
		$this->load->database();

		//加载admin模型
		$this->load->model('admin/Patient_model','patient');
		$this->load->model('admin/DoctorJ_model','doctorJ');
		//加载表单验证规则
		$this->load->library('form_validation');
		date_default_timezone_set("PRC");

	}

	public function getPatient()
	{
		//设置验证规则
		$this->form_validation->set_rules('openid','医生id','required');
		$this->form_validation->set_rules('session','session','required');

		//$this->form_validation->set_rules('startPage','startPage','required');
		//$this->form_validation->set_rules('howPage','howPage','required');

	    //先验证规则
		if($this->form_validation->run()==false){
				$result=array(
						'isSuccess'=>'0',
						'msg'=>'缺失了参数，请确认'
				);
				$this->printJson($result);
			exit();
		}

		$openid=$this->input->post('openid',TRUE);
		$session=$this->input->post('session',TRUE);

		//这里的session先不做验证
		//echo $this->session->userdata('fresh');

		//根据openid先来获取这个医生的id
		$data['DoctorOne']=$this->doctorJ->getOneByOpenid($openid);

		$dataDoctor=$this->patient->getAll2($data['DoctorOne']['id']);

		if(!empty($dataDoctor)){
			$result2=array();

			foreach ($dataDoctor as $key => $value) {
			$result2["isSuccess"]='1';
			$result2["msg"]='获取成功';
			$result2["patientinfo"][$key]['id']=urlencode($value['id']);
			$result2["patientinfo"][$key]['mobile']=urlencode($value['mobile']);
			$result2["patientinfo"][$key]['name']=urlencode($value['name']);
			$result2["patientinfo"][$key]['sex']=urlencode($value['sex']);
			$result2["patientinfo"][$key]['binzhong']=urlencode($value['binzhong']);
			$result2["patientinfo"][$key]['status']=urlencode($value['status']);
			$result2["patientinfo"][$key]['beizhu']=urlencode($value['beizhu']);
			$result2["patientinfo"][$key]['dateTime']=urlencode($value['dateTime']);
			}

			echo urldecode( json_encode ( $result2 )) ;
		}else{

			$result=array(
						'isSuccess'=>'0',
						'msg'=>'这个openid获取不到患者，可能患者为空，再试试'
				);
				$this->printJson($result);
		}
		
	}

	//测试之用
	public function test()
	{
		$this->load->view("api/form.html");

	}

	public function printJson($result){
		foreach ( $result as $key => $value ) {
					$result[$key] = urlencode ( $value );
				}
				echo urldecode ( json_encode ( $result ) );
	}

	public function ToOne(&$result)
	{
		foreach ( $result as $key => $value ) {
					$result[$key] = urlencode ( $value );
				}
	}

	public function addPatient(){
		//设置验证规则
		$this->form_validation->set_rules('name','姓名','required');
		$this->form_validation->set_rules('mobile','手机号码','required');
		// $this->form_validation->set_rules('sex','性别','required');
		// $this->form_validation->set_rules('binzhong','病种','required');
		// $this->form_validation->set_rules('status','状态','required');
		// $this->form_validation->set_rules('beizhu','备注','required');
		$this->form_validation->set_rules('openid','openid','required');
		$this->form_validation->set_rules('session','session','required');
		//先验证规则
		if($this->form_validation->run()==false){
				$result=array(
						'isSuccess'=>'0',
						'msg'=>'缺失了参数，请确认'
				);
				$this->printJson($result);
			exit();
		}

		//从openid获取医生doctorid
		//根据openid先来获取这个医生的id
		$openid=$this->input->post('openid',TRUE);
		$data2['DoctorOne']=$this->doctorJ->getOneByOpenid($openid);

		$data['mobile']=$this->input->post('mobile',TRUE);
		//验证手机号是否已经被添加过
		//验证手机号码是否存在
		if($this->patient->cheAdminByNP($data['mobile'])){
			$result=array(
						'isSuccess'=>'2',
						'msg'=>'添加失败，患者的手机号重复'
				);
			$this->printJson($result);	  
			exit();
		}

		$data['name']=$this->input->post('name',TRUE);
		$data['sex']=$this->input->post('sex',TRUE);
		$data['binzhong']=$this->input->post('binzhong',TRUE);
		$data['status']=$this->input->post('status',TRUE);
		$data['beizhu']=$this->input->post('beizhu',TRUE);
		$data['doctorid']=$data2['DoctorOne']['id'];
		$data['type']='单条添加';
		$data['dateTime']=$data['dateTime']='20'.date("y-m-d",time());
		$session=$this->input->post('session',TRUE);

		//这里还差session验证。
		if($this->patient->addPatientOne($data)){
				$result=array(
						'isSuccess'=>'1',
						'msg'=>'添加成功'
				);
				$this->printJson($result);	  	
		}else{
				$result=array(
						'isSuccess'=>'3',
						'msg'=>'添加失败'
				);
				$this->printJson($result);			  
		}	
	}

	//删除一条患者
	public function deleteOne(){

		$this->form_validation->set_rules('patientid','姓名','required');
		$this->form_validation->set_rules('session','session','required');
		//先验证规则
		if($this->form_validation->run()==false){
				$result=array(
						'isSuccess'=>'0',
						'msg'=>'缺失了参数，请确认'
				);
				$this->printJson($result);
			exit();
		}

		$data['patientid']=$this->input->post('patientid',TRUE);
		$data['session']=$this->input->post('session',TRUE);

		//这里需要验证session
		if($this->patient->deleteDoctorOne($data['patientid']))
		{
			$result=array(
						'isSuccess'=>'1',
						'msg'=>'删除成功'
				);
			$this->printJson($result);
		}else{
			$result=array(
						'isSuccess'=>'0',
						'msg'=>'删除失败'
				);
			$this->printJson($result);
		}
	}


	public function updatePatient(){

		$this->form_validation->set_rules('name','姓名','required');
		$this->form_validation->set_rules('mobile','手机号码','required');
		// $this->form_validation->set_rules('sex','性别','required');
		// $this->form_validation->set_rules('binzhong','病种','required');
		// $this->form_validation->set_rules('status','状态','required');
		// $this->form_validation->set_rules('beizhu','备注','required');
		$this->form_validation->set_rules('session','session','required');
		$this->form_validation->set_rules('patientId','patientId','required');
		//先验证规则
		if($this->form_validation->run()==false){
				$result=array(
						'isSuccess'=>'0',
						'msg'=>'缺失了参数，请确认'
				);
				$this->printJson($result);
			exit();
		}
		
		$data['name']=$this->input->post('name',TRUE);
		$data['mobile']=$this->input->post('mobile',TRUE);
		$data['sex']=$this->input->post('sex',TRUE);
		$data['beizhu']=$this->input->post('beizhu',TRUE);
		$data['binzhong']=$this->input->post('binzhong',TRUE);
		$data['status']=$this->input->post('status',TRUE);
		$session=$this->input->post('session',TRUE);
		$patientId=$this->input->post('patientId',TRUE);

		//exit();
		if($this->patient->updateOne($patientId,$data)){
			$result=array(
						'isSuccess'=>'1',
						'msg'=>'修改成功'
				);
			$this->printJson($result);
		}
		else {
			$result=array(
						'isSuccess'=>'0',
						'msg'=>'修改错误，请联系开发员查看问题'
				);
			$this->printJson($result);
		}
	}


	public function updateDoctor(){

		$this->form_validation->set_rules('openid','openid','required');
		// $this->form_validation->set_rules('mobileJ','手机号码','required');
		// $this->form_validation->set_rules('realNameJ','姓名','required');
		// $this->form_validation->set_rules('groupJ','分组','required');
		// $this->form_validation->set_rules('hostpitalJ','医院','required');
		// $this->form_validation->set_rules('deparJ','科室','required');
		$this->form_validation->set_rules('session','session','required');

		if($this->form_validation->run()==false){
				$result=array(
						'isSuccess'=>'0',
						'msg'=>'缺失了参数，请确认'
				);
				$this->printJson($result);
			exit();
		}
		//获取Doctorid
		$openid=$this->input->post('openid',TRUE);
		$data['DoctorOne']=$this->doctorJ->getOneByOpenid($openid);//$data['DoctorOne']['id']
	
		$id=$data['DoctorOne']['id'];
		$data1['mobileJ']=$this->input->post('mobileJ',TRUE);
		$data1['realNameJ']=$this->input->post('realNameJ',TRUE);
		$data1['groupJ']=$this->input->post('groupJ',TRUE);
		$data1['hostpitalJ']=$this->input->post('hostpitalJ',TRUE);
		$data1['deparJ']=$this->input->post('deparJ',TRUE);

		$session=$this->input->post('session',TRUE);
		 if($this->doctorJ->updateOne($id,$data1)){
			$result=array(
						'isSuccess'=>'1',
						'msg'=>'修改成功'
				);
			$this->printJson($result);
		}
		else {
			$result=array(
						'isSuccess'=>'2',
						'msg'=>'修改失败'
				);
			$this->printJson($result);
			
		}

	}

}
