<?php
/**
 * 购买管理
 */
defined('BASEPATH') OR exit("No direct script access allowed");

class BuyMan extends CI_Controller{

	//构造初始化函数
	public function __construct(){
		parent::__construct();

		//加载数据库
		$this->load->database();

		// //加载admin模型
		$this->load->model('admin/Buy_model','Buy');

		$this->checkLogStatus();
		date_default_timezone_set("PRC");
	}

		/*
	 * 检查登陆状态
	 */
	public function checkLogStatus()
	{
		if (!$this->session->userdata('doctorName')){
			redirect('doctor/Login');
		}
	}

	public function index($id=0){

		$data['buInfo']=$this->Buy->doctorGetBuyAll($id);
		$data['mobile']=array("mobile"=>$id);
		$this->testexcel1($data['buInfo']);
		$this->load->view("admin/doctorBuy.html",$data);
	}

	public function Tisearch(){
		$time=$this->input->get('info',TRUE);
		$id=$this->input->get('id',TRUE);
		$startTime=substr($time,6,4).'-'.substr($time,0,2).'-'.substr($time,3,2);
		$endTime=substr($time,19,4 ).'-'.substr($time, 13,2).'-'.substr($time,16,2);
		$data['buInfo']=$this->Buy->DoctorGetAllByTime($startTime,$endTime,$id);
		$data['mobile']=array("mobile"=>$id);
		$this->testexcel1($data['buInfo']);
		$this->load->view("admin/doctorBuy.html",$data);
	}

	public function Tisearch2(){
		$time=$this->input->get('info',TRUE);
		$startTime=substr($time,6,4).'-'.substr($time,0,2).'-'.substr($time,3,2);
		$endTime=substr($time,19,4 ).'-'.substr($time, 13,2).'-'.substr($time,16,2);
		$data['buInfo']=$this->Buy->DoctorGetAllByTime2($startTime,$endTime,$this->session->userdata('doctorId'));
		$this->testexcel($data['buInfo']);
		$this->load->view("admin/doctorBuy2.html",$data);
	}

	public function Nasearch(){
		$name=$this->input->get('info',TRUE);

		$data['buInfo']=$this->Buy->DoctorGetByName($name,$this->session->userdata('doctorId'));
		$this->testexcel($data['buInfo']);
		$this->load->view("admin/doctorBuy2.html",$data);
	}

	public function Mosearch(){
		$name=$this->input->get('info',TRUE);

		$data['buInfo']=$this->Buy->DoctorGetByMobile($name,$this->session->userdata('doctorId'));
		$this->testexcel($data['buInfo']);
		$this->load->view("admin/doctorBuy2.html",$data);
	}

	public function indexAll(){
		$data['buInfo']=$this->Buy->doctorGetBuyAll2($this->session->userdata('doctorId'));
		$this->testexcel($data['buInfo']);
		$this->load->view("admin/doctorBuy2.html",$data);
	}

	public function testexcel($data){
		require "assets/PHPExcel/PHPExcel.php";
		$excel = new PHPExcel();
		$letter = array('A','B','C','D','E');
		$tableheader = array('购买日期','姓名','手机号','产品','数量');
		for($i = 0;$i < count($tableheader);$i++) {
			$excel->getActiveSheet()->setCellValue("$letter[$i]1","$tableheader[$i]");
		}

		foreach ($data as $k => $v) {
			$i=$k+2;
			$excel->getActiveSheet()->setCellValue("$letter[0]$i",$v['timeb']);
			$excel->getActiveSheet()->setCellValue("$letter[1]$i",$v['name']);
			$excel->getActiveSheet()->setCellValue("$letter[2]$i",$v['mobil']);
			$excel->getActiveSheet()->setCellValue("$letter[3]$i",$v['product']);
			$excel->getActiveSheet()->setCellValue("$letter[4]$i",$v['number']);
		}

			$write = new PHPExcel_Writer_Excel5($excel);

			// header("Pragma: public");

			// header("Expires: 0");

			// header("Cache-Control:must-revalidate, post-check=0, pre-check=0");

			// header("Content-Type:application/force-download");

			// header("Content-Type:application/vnd.ms-execl");

			// header("Content-Type:application/octet-stream");

			// header("Content-Type:application/download");;

			// header('Content-Disposition:attachment;filename="testdata.xls"');

			// header("Content-Transfer-Encoding:binary");
			$filePath='/data/home/qxu2062660155/htdocs/1/assets/upload/'.$this->session->userdata('doctorName').'_dbuy1.xls';
			$write->save($filePath);
	}

	public function testexcel1($data){
		require "assets/PHPExcel/PHPExcel.php";
		$excel = new PHPExcel();
		$letter = array('A','B','C','D','E');
		$tableheader = array('购买日期','姓名','手机号','产品','数量');
		for($i = 0;$i < count($tableheader);$i++) {
			$excel->getActiveSheet()->setCellValue("$letter[$i]1","$tableheader[$i]");
		}

		foreach ($data as $k => $v) {
			$i=$k+2;
			$excel->getActiveSheet()->setCellValue("$letter[0]$i",$v['timeb']);
			$excel->getActiveSheet()->setCellValue("$letter[1]$i",$v['name']);
			$excel->getActiveSheet()->setCellValue("$letter[2]$i",$v['mobil']);
			$excel->getActiveSheet()->setCellValue("$letter[3]$i",$v['product']);
			$excel->getActiveSheet()->setCellValue("$letter[4]$i",$v['number']);
		}

			$write = new PHPExcel_Writer_Excel5($excel);

			// header("Pragma: public");

			// header("Expires: 0");

			// header("Cache-Control:must-revalidate, post-check=0, pre-check=0");

			// header("Content-Type:application/force-download");

			// header("Content-Type:application/vnd.ms-execl");

			// header("Content-Type:application/octet-stream");

			// header("Content-Type:application/download");;

			// header('Content-Disposition:attachment;filename="testdata.xls"');

			// header("Content-Transfer-Encoding:binary");
			$filePath='/data/home/qxu2062660155/htdocs/1/assets/upload/'.$this->session->userdata('doctorName').'_dbuy2.xls';
			$write->save($filePath);
	}
}
