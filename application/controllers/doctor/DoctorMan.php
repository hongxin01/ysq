<?php
/**
 * 后台管理员登录
 */
defined('BASEPATH') OR exit("No direct script access allowed");

class DoctorMan extends CI_Controller{


	//构造初始化函数
	public function __construct(){
		parent::__construct();

		//加载数据库
		$this->load->database();

	    //加载admin模型
		$this->load->model('admin/DoctorJ_model','doctorj');
		$this->load->model('admin/Doctor_model','doctor');

		//加载表单验证规则
		$this->load->library('form_validation');
		$this->checkLogStatus();
		date_default_timezone_set("PRC");
	}

	/*
	 * 检查登陆状态
	 */
	public function checkLogStatus()
	{
		if (!$this->session->userdata('doctorName')){
			redirect('doctor/Login');
		}
	} 

	//编辑一条记录(懒得改方法名字)
	public function editorOneDoc(){
		$data['DoctorOne']=$this->doctorj->getDoctorOne($this->session->userdata('doctorId'));
		//echo $id;
		//print_r($data);
		$this->load->view("admin/DoctorEditor.html",$data);
	}


	/*
	 * 更新一条数据
	 */
	public function updateGroupOne($id=1){
		$data['mobileJ']=$this->input->post('mobileJ',TRUE);
		$data['realNameJ']=$this->input->post('realname',TRUE);
		$data['groupJ']=$this->input->post('groupJ',TRUE);
		$data['hostpitalJ']=$this->input->post('hostpitalJ',TRUE);
		$data['deparJ']=$this->input->post('deparJ',TRUE);
		$pwd['pwd']=$this->input->post('pwd');
		if(!empty($pwd['pwd'])){
			
				if($this->doctor->updateOne2($this->session->userdata('doctorId'),$pwd)){
				}
				else {
					echo '修改错误，请联系开发员查看问题';
				}
		}
		if($this->doctorj->updateOne($id,$data)){
			echo "<script>alert('修改成功');location.href ='http://www.yishengqiao.com/1/index.php/doctor/DoctorMan/editorOneDoc';</script>";
			
		}
		else {
			echo '修改错误，请联系开发员查看问题';
		}
		
	}

	//测试之用
	public function test()
	{
	  
	}

}