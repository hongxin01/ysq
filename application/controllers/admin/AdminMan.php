<?php
/**
 * 购买管理
 */
defined('BASEPATH') OR exit("No direct script access allowed");

class AdminMan extends CI_Controller{

	//构造初始化函数
	public function __construct(){
		parent::__construct();

		//加载数据库
		$this->load->database();

		// //加载admin模型
		$this->load->model('admin/Admin_model','admin');

		//加载表单验证规则
		$this->load->library('form_validation');

		$this->checkLogStatus();
		date_default_timezone_set("PRC");

	}

		/*
	 * 检查登陆状态
	 */
	public function checkLogStatus()
	{
		if (!$this->session->userdata('admin')){
			redirect('admin/Login');
		}
	}

	//编辑一条记录(懒得改方法名字)
	public function editorOneDoc(){


		$data['DoctorOne']=$this->admin->getDoctorOne($this->session->userdata('admin'));
		//echo $id;
		//print_r($data);
		$this->load->view("admin/adminEditor.html",$data);

	}

	/*
	 * 更新一条数据
	 */
	public function updateGroupOne($id=1){

		$data['pwd']=$this->input->post('text',TRUE);

		if(strlen($data['pwd'])==0){
			echo "<script>alert('修改成功');</script>";
			$a=base_url("assets/error/adminUS.html");
			header("Location: $a"); 
	  		exit();
		}
		//print_r($data);
		if($this->admin->updateOne($id,$data)){
			echo "<script>alert('修改成功');</script>";
			$a=base_url("assets/error/adminUS.html");
			header("Location: $a"); 
	  		exit();
		}
		else {
			echo "<script>alert('修改错误');</script>";
		}
		
	}

	

}
