<?php
/**
 * 后台管理员登录
 */
defined('BASEPATH') OR exit("No direct script access allowed");

class LogAll extends CI_Controller{


	//构造初始化函数
	public function __construct(){
		parent::__construct();

		//加载数据库
		$this->load->database();

		//加载admin模型
		$this->load->model('admin/LogAll_model','logAll');

		//加载表单验证规则
		$this->load->library('form_validation');

		$this->checkLogStatus();
		date_default_timezone_set("PRC");
	}


	/*
	 * 检查登陆状态
	 */
	public function checkLogStatus()
	{
		if (!$this->session->userdata('admin')){
			redirect('admin/Login');
		}
	}

	//医生列表详细信息
	public function index()
	{
		$data['dataDoctor']=$this->logAll->getLogAll();
		$this->testexcel($data['dataDoctor']);
		$this->load->view("admin/LogAll.html",$data);
	}

	//医生名称搜索
	public function Dosearch(){
		$name=$this->input->get('info',TRUE);

		$data['dataDoctor']=$this->logAll->getDoByName($name);
		$this->testexcel($data['dataDoctor']);
		$this->load->view("admin/LogAll.html",$data);
	}

	public function testexcel($data){
		require "assets/PHPExcel/PHPExcel.php";
		$excel = new PHPExcel();
		$letter = array('A','B','C');
		$tableheader = array('操作人','操作','时间');
		for($i = 0;$i < count($tableheader);$i++) {
			$excel->getActiveSheet()->setCellValue("$letter[$i]1","$tableheader[$i]");
		}

		foreach ($data as $k => $v) {
			$i=$k+2;
			$excel->getActiveSheet()->setCellValue("$letter[0]$i",$v['person']);
			$excel->getActiveSheet()->setCellValue("$letter[1]$i",$v['operation']);
			$excel->getActiveSheet()->setCellValue("$letter[2]$i",$v['timeb']);
		}

			$write = new PHPExcel_Writer_Excel5($excel);

			// header("Pragma: public");

			// header("Expires: 0");

			// header("Cache-Control:must-revalidate, post-check=0, pre-check=0");

			// header("Content-Type:application/force-download");

			// header("Content-Type:application/vnd.ms-execl");

			// header("Content-Type:application/octet-stream");

			// header("Content-Type:application/download");;

			// header('Content-Disposition:attachment;filename="testdata.xls"');

			// header("Content-Transfer-Encoding:binary");
			$filePath='/data/home/qxu2062660155/htdocs/1/assets/upload/'.$this->session->userdata('admin').'_log.xls';
			$write->save($filePath);
	}

}