<?php
/**
 * 后台管理员登录
 */
defined('BASEPATH') OR exit("No direct script access allowed");

class DoctorMan extends CI_Controller{


	//构造初始化函数
	public function __construct(){
		parent::__construct();

		//加载数据库
		$this->load->database();

	    //加载admin模型
		$this->load->model('admin/Doctor_model','doctor');
		$this->load->model('admin/Group_model','group');
		$this->load->model('admin/LogAll_model','logAll');

		//加载表单验证规则
		$this->load->library('form_validation');

		$this->checkLogStatus();
		date_default_timezone_set("PRC");
	}

	public function checkLogStatus()
	{
		if (!$this->session->userdata('admin')){
			redirect('admin/Login');
		}
	}

	//医生列表详细信息
	public function index()
	{
		//获取所有信息
		$data['dataDoctor']=$this->doctor->getDoctorAll();
		$this->testexcel($data['dataDoctor']);
		//获取分组信息
		$data['dataGroup']=$this->group->getGroupAll();

		//print_r($data);
		//echo site_url('Login/index');
		$this->load->view("admin/Doctor.html",$data);
	}

	//医生名称搜索
	public function Dosearch(){
		$name=$this->input->get('info',TRUE);
		
		//echo urldecode($name);
		 $data['dataDoctor']=$this->doctor->getDoByName($name);
		 $this->testexcel($data['dataDoctor']);
		 //获取分组信息
		$data['dataGroup']=$this->group->getGroupAll();

		 $this->load->view("admin/Doctor.html",$data);
	}

	public function GroupSearch(){
		$id=$this->input->get('info',TRUE);

 		$data['dataDoctor']=$this->doctor->getDoByGroup($id);
 		$this->testexcel($data['dataDoctor']);
 	 	//获取分组信息
		$data['dataGroup']=$this->group->getGroupAll();
 		$this->load->view("admin/Doctor.html",$data);
	}

	public function Tisearch()
	{

		$time=$this->input->get('info',TRUE); //02/01/2016 - 02/26/2016
		$startTime=substr($time,6,4).'-'.substr($time,0,2).'-'.substr($time,3,2);
		$endTime=substr($time,19,4 ).'-'.substr($time, 13,2).'-'.substr($time,16,2);
		
		$data['dataDoctor']=$this->doctor->getDoByTime($startTime,$endTime);
		$this->testexcel($data['dataDoctor']);

			 //获取分组信息
		$data['dataGroup']=$this->group->getGroupAll();
		
		$this->load->view("admin/Doctor.html",$data);

	}

	//创建医生账号
	public function addDoctor()
	{
		//设置验证规则
		$this->form_validation->set_rules('loginName','姓名','required');
		$this->form_validation->set_rules('realName','姓名','required');
		$this->form_validation->set_rules('mobile','姓名','required');
		$this->form_validation->set_rules('hostpital','姓名','required');
		$this->form_validation->set_rules('depart','姓名','required');
		$this->form_validation->set_rules('group','分组','required');

	    //先验证规则
		if($this->form_validation->run()==false){
			echo validation_errors();
			exit();
		}

		$data['loginName']=$this->input->post('loginName',TRUE);
		$data['pwd']="888888";
		$data['mobile']=$this->input->post('mobile',TRUE);
		$data['realName']=$this->input->post('realName',TRUE);
		$data['group']=$this->input->post('group',TRUE);
		$data['hostpital']=$this->input->post('hostpital',TRUE);
		$data['depar']=$this->input->post('depart',TRUE);
		$data['dateTime']='20'.date("y-m-d",time());
		$aa=$data['loginName'];

		if($this->doctor->addDoctorByN($aa,$data)){
			$logData["operation"]="在医生管理中创建了一条数据";
			$logData["timeb"]='20'.date("y-m-d",time());
			$logData["person"]=$this->session->userdata('admin');
			$this->logAll->addOne($logData);
		  	echo 1;
		}else{
		  	echo 0;
		}
	}

	//删除一条记录 
	public function deleteOne()
	{
		//设置验证规则
		$this->form_validation->set_rules('DoctorId','参数','required');
		 //先验证规则
		if($this->form_validation->run()==false){
			echo validation_errors();
			exit();
		}
		$data['id']=$this->input->post('DoctorId',TRUE);
		
		if($this->doctor->deleteDoctorOne($data['id']))
		{

			$logData["operation"]="在医生管理中删除了一条数据，医生id是".$data['id'];
			$logData["timeb"]='20'.date("y-m-d",time());
			$logData["person"]=$this->session->userdata('admin');
			$this->logAll->addOne($logData);

			echo 1;
		}else{
			echo 0;
		}

	}
	
	//编辑一条记录
	public function editorOneDoc($id=1){

		$data['DoctorOne']=$this->doctor->getDoctorOne($id);
		$data['dataGroup']=$this->group->getGroupAll();
		//print_r($data['DoctorOne']);
		$this->load->view("admin/Editor.html",$data);
	}

	/*
	 * 更新一条数据
	 */
	public function updateGroupOne($id=1){
		$data['loginName']=$this->input->post('loginName',TRUE);
		$data['realName']=$this->input->post('realName',TRUE);
		$data['mobile']=$this->input->post('mobile',TRUE);
		$data['hostpital']=$this->input->post('hostpital',TRUE);
		$data['depar']=$this->input->post('depar',TRUE);
		$data['group']=$this->input->post('group',TRUE);

		if($this->doctor->updateOne($id,$data)){

			$logData["operation"]="在医生管理中更新了一条数据，数据id是".$id;
			$logData["timeb"]='20'.date("y-m-d",time());
			$logData["person"]=$this->session->userdata('admin');
			$this->logAll->addOne($logData);

			redirect("admin/DoctorMan/index");
		} 
		else {
			echo '修改错误，请联系开发员查看问题';
		}
	}

	//测试之用
	public function test()
	{
	  
	}

	public function testexcel($data){
		require "assets/PHPExcel/PHPExcel.php";
		$excel = new PHPExcel();
		$letter = array('A','B','C','D','E','F','G','H');
		$tableheader = array('姓名','账号','手机号','医院','科室','微信号','创建时间','分组');
		for($i = 0;$i < count($tableheader);$i++) {
			$excel->getActiveSheet()->setCellValue("$letter[$i]1","$tableheader[$i]");
		}

		foreach ($data as $k => $v) {
			$i=$k+2;
			$excel->getActiveSheet()->setCellValue("$letter[0]$i",$v['realName']);
			$excel->getActiveSheet()->setCellValue("$letter[1]$i",$v['loginName']);
			$excel->getActiveSheet()->setCellValue("$letter[2]$i",$v['mobile']);
			$excel->getActiveSheet()->setCellValue("$letter[3]$i",$v['hostpital']);
			$excel->getActiveSheet()->setCellValue("$letter[4]$i",$v['depar']);
			$excel->getActiveSheet()->setCellValue("$letter[5]$i",$v['weichat']);
			$excel->getActiveSheet()->setCellValue("$letter[6]$i",$v['dateTime']);
			$excel->getActiveSheet()->setCellValue("$letter[7]$i",$v['name']);
		}

			$write = new PHPExcel_Writer_Excel5($excel);

			// header("Pragma: public");

			// header("Expires: 0");

			// header("Cache-Control:must-revalidate, post-check=0, pre-check=0");

			// header("Content-Type:application/force-download");

			// header("Content-Type:application/vnd.ms-execl");

			// header("Content-Type:application/octet-stream");

			// header("Content-Type:application/download");;

			// header('Content-Disposition:attachment;filename="testdata.xls"');

			// header("Content-Transfer-Encoding:binary");
			$filePath='/data/home/qxu2062660155/htdocs/1/assets/upload/'.$this->session->userdata('admin').'_doctor.xls';
			$write->save($filePath);
	}

}