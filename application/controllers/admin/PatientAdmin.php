<?php
/**
 * 后台管理员登录
 */
defined('BASEPATH') OR exit("No direct script access allowed");

class PatientAdmin extends CI_Controller{


	//构造初始化函数
	public function __construct(){
		parent::__construct();

		//加载数据库
		$this->load->database();

		//加载admin模型
		$this->load->model('admin/Patient_model','patient');
		$this->load->model('admin/LogAll_model','logAll');
		//加载表单验证规则
		$this->load->library('form_validation');

		$this->checkLogStatus();
		date_default_timezone_set("PRC");
	}


	/*
	 * 检查登陆状态
	 */
	public function checkLogStatus()
	{
		if (!$this->session->userdata('admin')){
			redirect('admin/Login');
		}
	}

	//医生列表详细信息
	public function indexOne()
	{
		$data['dataDoctor']=$this->patient->getAll4();
		$this->testexcel($data['dataDoctor']);
		$this->load->view("admin/PatientAdmin.html",$data);
	}


	public function indexPl(){
		$data['dataDoctor']=$this->patient->getAll8();
		$this->testexcel2($data['dataDoctor']);
		$this->load->view("admin/PatientAdminPL.html",$data);
	}


	public function deleteOne(){
		//设置验证规则
		$this->form_validation->set_rules('DoctorId','参数','required');
		 //先验证规则
		if($this->form_validation->run()==false){
			echo validation_errors();
			exit();
		}
		$data['id']=$this->input->post('DoctorId',TRUE);
		if($this->patient->deleteDoctorOne($data['id']))
		{

			$logData["operation"]="管理员在患者管理中删除了一条数据，数据id是".$data['id'];
			$logData["timeb"]='20'.date("y-m-d",time());
			$logData["person"]=$this->session->userdata('admin');
			$this->logAll->addOne($logData);

			echo 1;
		}else{
			echo 0;
		}

	}

	public function testexcel($data){
		require "assets/PHPExcel/PHPExcel.php";
		$excel = new PHPExcel();
		$letter = array('A','B','C','D','E','F','G','H','I');
		$tableheader = array('姓名','手机号','性别','病种','状态','创建时间','医生姓名','医生账号','医生号码');
		for($i = 0;$i < count($tableheader);$i++) {
			$excel->getActiveSheet()->setCellValue("$letter[$i]1","$tableheader[$i]");
		}

		foreach ($data as $k => $v) {
			$i=$k+2;
			$excel->getActiveSheet()->setCellValue("$letter[0]$i",$v['name']);
			$excel->getActiveSheet()->setCellValue("$letter[1]$i",$v['pmobile']);
			$excel->getActiveSheet()->setCellValue("$letter[2]$i",$v['sex']);
			$excel->getActiveSheet()->setCellValue("$letter[3]$i",$v['binzhong']);
			$excel->getActiveSheet()->setCellValue("$letter[4]$i",$v['status']);
			$excel->getActiveSheet()->setCellValue("$letter[5]$i",$v['dateTime']);
			if(!is_null($v['realName'])){
				$excel->getActiveSheet()->setCellValue("$letter[6]$i",$v['realName']);
                
              }else{
              	$excel->getActiveSheet()->setCellValue("$letter[6]$i",$v['realNameJ']);
              }
			if(!is_null($v['loginName'])){
				$excel->getActiveSheet()->setCellValue("$letter[7]$i",$v['loginName']);
                               }else{
                $excel->getActiveSheet()->setCellValue("$letter[7]$i","未绑定");
             }
          	 if(!is_null($v['mobile'])){
           		$excel->getActiveSheet()->setCellValue("$letter[8]$i",$v['mobile']);
                    }else{
               $excel->getActiveSheet()->setCellValue("$letter[8]$i",$v['mobileJ']);
            }
		}

		

			$write = new PHPExcel_Writer_Excel5($excel);

			// header("Pragma: public");

			// header("Expires: 0");

			// header("Cache-Control:must-revalidate, post-check=0, pre-check=0");

			// header("Content-Type:application/force-download");

			// header("Content-Type:application/vnd.ms-execl");

			// header("Content-Type:application/octet-stream");

			// header("Content-Type:application/download");;

			// header('Content-Disposition:attachment;filename="testdata.xls"');

			// header("Content-Transfer-Encoding:binary");

			$filePath='/data/home/qxu2062660155/htdocs/1/assets/upload/'.$this->session->userdata('admin').'_patiento.xls';
			$write->save($filePath);
	}

	public function testexcel2($data){
		require "assets/PHPExcel/PHPExcel.php";
		$excel = new PHPExcel();
		$letter = array('A','B','C','D','E','F','G','H','I');
		$tableheader = array('姓名','手机号','性别','病种','状态','创建时间','医生姓名','医生账号','医生号码');
		for($i = 0;$i < count($tableheader);$i++) {
			$excel->getActiveSheet()->setCellValue("$letter[$i]1","$tableheader[$i]");
		}

		foreach ($data as $k => $v) {
			$i=$k+2;
			$excel->getActiveSheet()->setCellValue("$letter[0]$i",$v['name']);
			$excel->getActiveSheet()->setCellValue("$letter[1]$i",$v['pmobile']);
			$excel->getActiveSheet()->setCellValue("$letter[2]$i",$v['sex']);
			$excel->getActiveSheet()->setCellValue("$letter[3]$i",$v['binzhong']);
			$excel->getActiveSheet()->setCellValue("$letter[4]$i",$v['status']);
			$excel->getActiveSheet()->setCellValue("$letter[5]$i",$v['dateTime']);
			if(!is_null($v['realName'])){
				$excel->getActiveSheet()->setCellValue("$letter[6]$i",$v['realName']);
                
              }else{
              	$excel->getActiveSheet()->setCellValue("$letter[6]$i",$v['realNameJ']);
              }
			if(!is_null($v['loginName'])){
				$excel->getActiveSheet()->setCellValue("$letter[7]$i",$v['loginName']);
                               }else{
                $excel->getActiveSheet()->setCellValue("$letter[7]$i","未绑定");
             }
          	 if(!is_null($v['mobile'])){
           		$excel->getActiveSheet()->setCellValue("$letter[8]$i",$v['mobile']);
                    }else{
               $excel->getActiveSheet()->setCellValue("$letter[8]$i",$v['mobileJ']);
            }
		}

		

			$write = new PHPExcel_Writer_Excel5($excel);

			// header("Pragma: public");

			// header("Expires: 0");

			// header("Cache-Control:must-revalidate, post-check=0, pre-check=0");

			// header("Content-Type:application/force-download");

			// header("Content-Type:application/vnd.ms-execl");

			// header("Content-Type:application/octet-stream");

			// header("Content-Type:application/download");;

			// header('Content-Disposition:attachment;filename="testdata.xls"');

			// header("Content-Transfer-Encoding:binary");

			$filePath='/data/home/qxu2062660155/htdocs/1/assets/upload/'.$this->session->userdata('admin').'_patienta.xls';
			$write->save($filePath);
	}


	public function editorIndexOne($id=1){

		$data['DoctorOne']=$this->patient->getPatientOne($id);
		$checkbz=array(
			'gxy'=>"",
			'tnb'=>"",
			'other'=>"",
			'otherValue'=>""
			);
		$gxy="";
		$tnb="";
		$other="";
		$otherValue="";

		$yjd="";
		$yqy="";
		$ysf="";
		$others="";
		$othersValue="";

		$binhzong=explode(',',$data['DoctorOne']['binzhong']);

		if(!empty($data['DoctorOne']['binzhong']))
		{
				foreach ($binhzong as $v) {
					if($v=="高血压"){
						$gxy="checked='checked'";
					}else if($v=="糖尿病"){
						$tnb="checked='checked'";
					}else{
						$other="checked='checked'";
						$otherValue=$v;
					}
				}
		}

		$status=explode(',',$data['DoctorOne']['status']);
		if(!empty($data['DoctorOne']['status']))
		{
			foreach ($status as $v) {
				if($v=="已建档"){
					$yjd="checked='checked'";
				}else if($v=="已签约"){
					$yqy="checked='checked'";
				}else if($v=="已随访"){
					$ysf="checked='checked'";
				}else{
					$others="checked='checked'";
					$othersValue=$v;
				}
			}
		}



		$aa="<div class='checkbox'><label><input name='checkbox1' $gxy id='gxyid' type='checkbox' class='ace' /><span class='lbl'> 高血压</span></label><label><input name='checkbox1' $tnb id='tnbid' type='checkbox' class='ace' /><span class='lbl'> 糖尿病</span></label><label><input name='checkbox1' id='qtid' $other type='checkbox' class='ace' /><span class='lbl'> 其它(请输入)：</span><input id='qtbzid' type='text' value='$otherValue' placeholder='添加其它病种'></label></div>";

		$bb="<div class='checkbox'><label><input name='checkbox2' $yjd id='yjdid' type='checkbox' class='ace' /><span class='lbl'> 已建档</span></label><label><input name='checkbox2' $yqy id='yqyid' type='checkbox' class='ace' /><span class='lbl'> 已签约</span></label><label><input name='checkbox2' id='ysfid' $ysf type='checkbox' class='ace' /><span class='lbl'> 已随访</span></label><label><input name='checkbox2' id='yqtid' $others type='checkbox' class='ace' /><span class='lbl'> 其他</span><input id='qtztid' type='text' value='$othersValue' placeholder='添加其它'></label></div>";																

		$data['binzhong']=$aa;
		$data['status']=$bb;
		$data['patientId']=$id;
		//print_r($data['binzhong']);
		//exit();
		$this->load->view("admin/PatientAdminOne.html",$data);

	}


	public function editorIndexPL($id=1){

		$data['DoctorOne']=$this->patient->getPatientOne($id);
		$checkbz=array(
			'gxy'=>"",
			'tnb'=>"",
			'other'=>"",
			'otherValue'=>""
			);
		$gxy="";
		$tnb="";
		$other="";
		$otherValue="";

		$yjd="";
		$yqy="";
		$ysf="";
		$others="";
		$othersValue="";

		$binhzong=explode(',',$data['DoctorOne']['binzhong']);

		if(!empty($data['DoctorOne']['binzhong']))
		{
				foreach ($binhzong as $v) {
					if($v=="高血压"){
						$gxy="checked='checked'";
					}else if($v=="糖尿病"){
						$tnb="checked='checked'";
					}else{
						$other="checked='checked'";
						$otherValue=$v;
					}
				}
		}

		$status=explode(',',$data['DoctorOne']['status']);
		if(!empty($data['DoctorOne']['status']))
		{
			foreach ($status as $v) {
				if($v=="已建档"){
					$yjd="checked='checked'";
				}else if($v=="已签约"){
					$yqy="checked='checked'";
				}else if($v=="已随访"){
					$ysf="checked='checked'";
				}else{
					$others="checked='checked'";
					$othersValue=$v;
				}
			}
		}



		$aa="<div class='checkbox'><label><input name='checkbox1' $gxy id='gxyid' type='checkbox' class='ace' /><span class='lbl'> 高血压</span></label><label><input name='checkbox1' $tnb id='tnbid' type='checkbox' class='ace' /><span class='lbl'> 糖尿病</span></label><label><input name='checkbox1' id='qtid' $other type='checkbox' class='ace' /><span class='lbl'> 其它(请输入)：</span><input id='qtbzid' type='text' value='$otherValue' placeholder='添加其它病种'></label></div>";

		$bb="<div class='checkbox'><label><input name='checkbox2' $yjd id='yjdid' type='checkbox' class='ace' /><span class='lbl'> 已建档</span></label><label><input name='checkbox2' $yqy id='yqyid' type='checkbox' class='ace' /><span class='lbl'> 已签约</span></label><label><input name='checkbox2' id='ysfid' $ysf type='checkbox' class='ace' /><span class='lbl'> 已随访</span></label><label><input name='checkbox2' id='yqtid' $others type='checkbox' class='ace' /><span class='lbl'> 其他</span><input id='qtztid' type='text' value='$othersValue' placeholder='添加其它'></label></div>";																

		$data['binzhong']=$aa;
		$data['status']=$bb;
		$data['patientId']=$id;
		//print_r($data['binzhong']);
		//exit();
		$this->load->view("admin/PatientAdminPLeditor.html",$data);

	}


	public function updatePatientOne(){

		$data['name']=$this->input->post('realName',TRUE);
		$data['sex']=$this->input->post('sex',TRUE);
		$data['beizhu']=$this->input->post('beizhu',TRUE);
		$binzhong=$this->input->post('bingzhong',TRUE);

		if(strlen($binzhong)==0)
		{
			$data['binzhong']="";
		}else{
			$data['binzhong']=substr($binzhong,0,strlen($binzhong)-1);
		}

		$status=$this->input->post('status',TRUE);
		if(strlen($status)==0)
		{
				$data['status']="";
		}else{
			$data['status']=substr($status,0,strlen($status)-1);
		}
		
		$id=$this->input->post('patientId',TRUE);

		if($this->patient->updateOne($id,$data)){

			$logData["operation"]="管理员在后台修改了一个患者，患者id是".$id;
			$logData["timeb"]='20'.date("y-m-d",time());
			$logData["person"]=$this->session->userdata('doctorName');
			$this->logAll->addOne($logData);
			echo '修改成功';
		}
		else {
			echo '修改错误，请联系开发员查看问题';
		}
	}

	public function searchMobile(){
		$name=$this->input->get('info',TRUE);
		 //获取所有信息
	    $data['dataDoctor']=$this->patient->getAll6($name);
	    $this->testexcel($data['dataDoctor']);
		$this->load->view("admin/PatientAdmin.html",$data);
		
	}

	public function searchMobile2(){
		$name=$this->input->get('info',TRUE);
		 //获取所有信息
	    $data['dataDoctor']=$this->patient->getAll7($name);
	     $this->testexcel($data['dataDoctor']);
		$this->load->view("admin/PatientAdmin.html",$data);
		
	}

	public function searchMobile3(){
		$name=$this->input->get('info',TRUE);
		 //获取所有信息
	    $data['dataDoctor']=$this->patient->getAll9($name);
	    $this->testexcel2($data['dataDoctor']);
		$this->load->view("admin/PatientAdminPL.html",$data);
		
	}

	public function searchMobile4(){
		$name=$this->input->get('info',TRUE);
		 //获取所有信息
	    $data['dataDoctor']=$this->patient->getAll10($name);
	    $this->testexcel2($data['dataDoctor']);
		$this->load->view("admin/PatientAdminPL.html",$data);
		
	}



}