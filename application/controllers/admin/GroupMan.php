<?php
/**
 * 后台管理员登录
 */
defined('BASEPATH') OR exit("No direct script access allowed");

class GroupMan extends CI_Controller{


	//构造初始化函数
	public function __construct(){
		parent::__construct();

		//加载数据库
		$this->load->database();

		//加载admin模型
		$this->load->model('admin/Group_model','group');
		$this->load->model('admin/LogAll_model','logAll');
		$this->load->model('admin/Doctor_model','doctor');
		//加载表单验证规则
		$this->load->library('form_validation');

		$this->checkLogStatus();
		date_default_timezone_set("PRC");

	}

	public function checkLogStatus()
	{
		if (!$this->session->userdata('admin')){
			redirect('admin/Login');
		}
	}


	public function index()
	{
		//获取所有信息
		$data['dataGroup']=$this->group->getGroupAll();
		$this->testexcel($data['dataGroup']);
		$this->load->view("admin/Group.html",$data);
	}

	//医生名称搜索
	public function Dosearch(){
		$name=$this->input->get('info',TRUE);
		
		//echo urldecode($name);
		 $data['dataGroup']=$this->group->getDoByName($name);
		 $this->testexcel($data['dataGroup']);
		 $this->load->view("admin/Group.html",$data);
	}

	//
	public function addGroup()
	{
		//设置验证规则
		$this->form_validation->set_rules('addDoctorValue','用户名','required');

	    //先验证规则
		if($this->form_validation->run()==false){
			echo validation_errors();
			exit();
		}

		$DoctorValue=$this->input->post('addDoctorValue',TRUE);

		$data['name']=$DoctorValue;
		//echo $data['name'];
		 if($this->group->addOne($data)){

		 	$logData["operation"]="在分组管理中添加了一个分组";
			$logData["timeb"]='20'.date("y-m-d",time());
			$logData["person"]=$this->session->userdata('admin');
			$this->logAll->addOne($logData);

		  	echo 1;
		}else{
		  	echo 0;
		}

	}


	//删除一条记录
	public function deleteOne()
	{
		//设置验证规则
		$this->form_validation->set_rules('DoctorId','参数','required');
		 //先验证规则
		if($this->form_validation->run()==false){
			echo validation_errors();
			exit();
		}
		$data['id']=$this->input->post('DoctorId',TRUE);


		if ($this->doctor->cheGroup($data['id'])){
			echo "该分组下有医生，若要删除该分组，请先将该分组医生全部分配到其他组";
			exit();
		}

		if($this->group->deleteDoctorOne($data['id']))
		{

			$logData["operation"]="在分组管理中删除了一个分组，分组的id是".$data['id'];
			$logData["timeb"]='20'.date("y-m-d",time());
			$logData["person"]=$this->session->userdata('admin');
			$this->logAll->addOne($logData);
			echo 1;
		}else{
			echo 0;
		}

	}

	//编辑一条记录
	public function editorOneDoc($id=1){

		$data['DoctorOne']=$this->group->getDoctorOne($id);
		//echo $id;
		//print_r($data);
		$this->load->view("admin/GroupEditor.html",$data);

	}

	/*
	 * 更新一条数据
	 */
	public function updateGroupOne($id=1){
		$data['name']=$this->input->post('text',TRUE);

		//print_r($data);
		if($this->group->updateOne($id,$data)){

			$logData["operation"]="在分组管理中更新了一个分组，分组的id是".$id;
			$logData["timeb"]='20'.date("y-m-d",time());
			$logData["person"]=$this->session->userdata('admin');
			$this->logAll->addOne($logData);

			redirect("admin/GroupMan/index");
		}
		else {
			echo '修改错误，请联系开发员查看问题';
		}
		
	}


	//测试之用
	public function test()
	{
		$code=strtolower($this->session->userdata('code'));
		echo $code;
	}

	public function testexcel($data){
		require "assets/PHPExcel/PHPExcel.php";
		$excel = new PHPExcel();
		$letter = array('A');
		$tableheader = array('分组名');
		for($i = 0;$i < count($tableheader);$i++) {
			$excel->getActiveSheet()->setCellValue("$letter[$i]1","$tableheader[$i]");
		}

		foreach ($data as $k => $v) {
			$i=$k+2;
			$excel->getActiveSheet()->setCellValue("$letter[0]$i",$v['name']);
		}

		$write = new PHPExcel_Writer_Excel5($excel);

		// header("Pragma: public");

		// header("Expires: 0");

		// header("Cache-Control:must-revalidate, post-check=0, pre-check=0");

		// header("Content-Type:application/force-download");

		// header("Content-Type:application/vnd.ms-execl");

		// header("Content-Type:application/octet-stream");

		// header("Content-Type:application/download");;

		// header('Content-Disposition:attachment;filename="testdata.xls"');

		// header("Content-Transfer-Encoding:binary");
		$filePath='/data/home/qxu2062660155/htdocs/1/assets/upload/'.$this->session->userdata('admin').'_Group.xls';
		$write->save($filePath);
	}

}
